var gulp = require('gulp'),
    svgSprite = require('gulp-svg-sprite');

var paths = {
  src: './src/',
  assets: './assets/',
  icons: './src/icons/'
};

gulp.task('sprites', function() {
  gulp.src('*.svg', {cwd: paths.icons})
    .pipe(svgSprite({
        shape: {
          dimension: {
            maxWidth: 20,
            maxHeight: 20
          }
        },
        mode: {
            symbol: {
              dest: '.',
              sprite: 'symbol.svg',
              inline: true
            }
        }
    }))
    .pipe(gulp.dest(paths.assets + 'img'));
});
