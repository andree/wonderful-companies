'use strict'

const Metalsmith = require('metalsmith')
const markdown = require('metalsmith-markdown')
const layouts = require('metalsmith-layouts')
const assets = require('metalsmith-assets')
const metadata = require('metalsmith-metadata')
const define = require('metalsmith-define')

let clearMetadata = function clearMetadata (opts) {
  return function (files, metalsmith, done) {
    let meta = metalsmith.metadata()

    for (var key in meta) {
      if (meta.hasOwnProperty(key)) {
        delete meta[key]
      }
    }

    return done()
  }
}

Metalsmith(__dirname)
  .source('src/')
  .destination('./build/')
  .use(clearMetadata())
  .use(define({
    heading: 'Heading title',
  }))
  .use(metadata({
    'ticker': 'data/ticker.json',
    'ticker2': 'data/ticker2.json',
    'ticker3': 'data/ticker3.json',
  }))
  .use(markdown())
  .use(layouts({
    engine: 'mustache',
    directory: 'src/layouts',
    partials: 'src/partials',
    default: 'layout.html',
    pattern: '**/*.html'
  }))
  .use(assets({
    source: './assets', // relative to the working directory
    destination: './assets' // relative to the build directory
  }))
  .build(function (error) {
    if (error) {
      console.log(error)
    }
  })